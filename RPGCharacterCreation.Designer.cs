﻿namespace RPG_Character_Generator
{
    partial class RPGCharacterCreation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RPGCharacterCreation));
            this.CharacterNameInput = new System.Windows.Forms.TextBox();
            this.CharacterCreateButton = new System.Windows.Forms.Button();
            this.CharacterNameValueLabel = new System.Windows.Forms.Label();
            this.CharacterTypeValueLabel = new System.Windows.Forms.Label();
            this.HpValueLabel = new System.Windows.Forms.Label();
            this.StrenghtValueLabel = new System.Windows.Forms.Label();
            this.CharismaValueLabel = new System.Windows.Forms.Label();
            this.WisdomValueLabel = new System.Windows.Forms.Label();
            this.CharacterNameLabel = new System.Windows.Forms.Label();
            this.CharacterTypeLabel = new System.Windows.Forms.Label();
            this.HpLabel = new System.Windows.Forms.Label();
            this.StrengtLabel = new System.Windows.Forms.Label();
            this.CharismaLabel = new System.Windows.Forms.Label();
            this.WisdomLabel = new System.Windows.Forms.Label();
            this.AgilityLabel = new System.Windows.Forms.Label();
            this.AgilityValueLabel = new System.Windows.Forms.Label();
            this.CharacterTypeComboBox = new System.Windows.Forms.ComboBox();
            this.CreateNewCharacterButton = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.LoadButton = new System.Windows.Forms.Button();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.UpdateNameButton = new System.Windows.Forms.Button();
            this.RenamingInputBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // CharacterNameInput
            // 
            this.CharacterNameInput.BackColor = System.Drawing.SystemColors.WindowText;
            this.CharacterNameInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.CharacterNameInput.ForeColor = System.Drawing.Color.Red;
            this.CharacterNameInput.Location = new System.Drawing.Point(43, 12);
            this.CharacterNameInput.Name = "CharacterNameInput";
            this.CharacterNameInput.Size = new System.Drawing.Size(206, 31);
            this.CharacterNameInput.TabIndex = 0;
            this.CharacterNameInput.Tag = "Character name...";
            this.CharacterNameInput.TextChanged += new System.EventHandler(this.CharacterNameInput_TextChanged);
            // 
            // CharacterCreateButton
            // 
            this.CharacterCreateButton.BackColor = System.Drawing.SystemColors.Desktop;
            this.CharacterCreateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.CharacterCreateButton.ForeColor = System.Drawing.Color.Red;
            this.CharacterCreateButton.Location = new System.Drawing.Point(265, 7);
            this.CharacterCreateButton.Name = "CharacterCreateButton";
            this.CharacterCreateButton.Size = new System.Drawing.Size(107, 41);
            this.CharacterCreateButton.TabIndex = 1;
            this.CharacterCreateButton.Text = "Create";
            this.CharacterCreateButton.UseVisualStyleBackColor = false;
            this.CharacterCreateButton.Click += new System.EventHandler(this.Create_Click);
            // 
            // CharacterNameValueLabel
            // 
            this.CharacterNameValueLabel.AutoSize = true;
            this.CharacterNameValueLabel.BackColor = System.Drawing.SystemColors.WindowText;
            this.CharacterNameValueLabel.Cursor = System.Windows.Forms.Cursors.PanEast;
            this.CharacterNameValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.CharacterNameValueLabel.ForeColor = System.Drawing.Color.Red;
            this.CharacterNameValueLabel.Location = new System.Drawing.Point(189, 57);
            this.CharacterNameValueLabel.MinimumSize = new System.Drawing.Size(10, 10);
            this.CharacterNameValueLabel.Name = "CharacterNameValueLabel";
            this.CharacterNameValueLabel.Size = new System.Drawing.Size(10, 25);
            this.CharacterNameValueLabel.TabIndex = 6;
            this.CharacterNameValueLabel.Visible = false;
            this.CharacterNameValueLabel.Click += new System.EventHandler(this.CharacterNameValueLabel_Click);
            // 
            // CharacterTypeValueLabel
            // 
            this.CharacterTypeValueLabel.AutoSize = true;
            this.CharacterTypeValueLabel.BackColor = System.Drawing.SystemColors.WindowText;
            this.CharacterTypeValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.CharacterTypeValueLabel.ForeColor = System.Drawing.Color.Red;
            this.CharacterTypeValueLabel.Location = new System.Drawing.Point(189, 96);
            this.CharacterTypeValueLabel.MinimumSize = new System.Drawing.Size(10, 10);
            this.CharacterTypeValueLabel.Name = "CharacterTypeValueLabel";
            this.CharacterTypeValueLabel.Size = new System.Drawing.Size(10, 25);
            this.CharacterTypeValueLabel.TabIndex = 7;
            this.CharacterTypeValueLabel.Visible = false;
            this.CharacterTypeValueLabel.Click += new System.EventHandler(this.CharacterTypeValueLabel_Click);
            // 
            // HpValueLabel
            // 
            this.HpValueLabel.AutoSize = true;
            this.HpValueLabel.BackColor = System.Drawing.SystemColors.WindowText;
            this.HpValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.HpValueLabel.ForeColor = System.Drawing.Color.Red;
            this.HpValueLabel.Location = new System.Drawing.Point(189, 139);
            this.HpValueLabel.MinimumSize = new System.Drawing.Size(10, 10);
            this.HpValueLabel.Name = "HpValueLabel";
            this.HpValueLabel.Size = new System.Drawing.Size(10, 25);
            this.HpValueLabel.TabIndex = 8;
            this.HpValueLabel.Visible = false;
            this.HpValueLabel.Click += new System.EventHandler(this.HpValueLabel_Click);
            // 
            // StrenghtValueLabel
            // 
            this.StrenghtValueLabel.AutoSize = true;
            this.StrenghtValueLabel.BackColor = System.Drawing.SystemColors.WindowText;
            this.StrenghtValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.StrenghtValueLabel.ForeColor = System.Drawing.Color.Red;
            this.StrenghtValueLabel.Location = new System.Drawing.Point(189, 176);
            this.StrenghtValueLabel.MinimumSize = new System.Drawing.Size(10, 10);
            this.StrenghtValueLabel.Name = "StrenghtValueLabel";
            this.StrenghtValueLabel.Size = new System.Drawing.Size(10, 25);
            this.StrenghtValueLabel.TabIndex = 9;
            this.StrenghtValueLabel.Visible = false;
            this.StrenghtValueLabel.Click += new System.EventHandler(this.StrenghtValueLabel_Click);
            // 
            // CharismaValueLabel
            // 
            this.CharismaValueLabel.AutoSize = true;
            this.CharismaValueLabel.BackColor = System.Drawing.SystemColors.WindowText;
            this.CharismaValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.CharismaValueLabel.ForeColor = System.Drawing.Color.Red;
            this.CharismaValueLabel.Location = new System.Drawing.Point(189, 214);
            this.CharismaValueLabel.MinimumSize = new System.Drawing.Size(10, 10);
            this.CharismaValueLabel.Name = "CharismaValueLabel";
            this.CharismaValueLabel.Size = new System.Drawing.Size(10, 25);
            this.CharismaValueLabel.TabIndex = 10;
            this.CharismaValueLabel.Visible = false;
            this.CharismaValueLabel.Click += new System.EventHandler(this.CharismaValueLabel_Click);
            // 
            // WisdomValueLabel
            // 
            this.WisdomValueLabel.AutoSize = true;
            this.WisdomValueLabel.BackColor = System.Drawing.SystemColors.WindowText;
            this.WisdomValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.WisdomValueLabel.ForeColor = System.Drawing.Color.Red;
            this.WisdomValueLabel.Location = new System.Drawing.Point(189, 251);
            this.WisdomValueLabel.MinimumSize = new System.Drawing.Size(10, 10);
            this.WisdomValueLabel.Name = "WisdomValueLabel";
            this.WisdomValueLabel.Size = new System.Drawing.Size(10, 25);
            this.WisdomValueLabel.TabIndex = 11;
            this.WisdomValueLabel.Visible = false;
            this.WisdomValueLabel.Click += new System.EventHandler(this.WisdomValueLabel_Click);
            // 
            // CharacterNameLabel
            // 
            this.CharacterNameLabel.AutoSize = true;
            this.CharacterNameLabel.BackColor = System.Drawing.SystemColors.WindowText;
            this.CharacterNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.CharacterNameLabel.ForeColor = System.Drawing.Color.Red;
            this.CharacterNameLabel.Location = new System.Drawing.Point(12, 57);
            this.CharacterNameLabel.MinimumSize = new System.Drawing.Size(10, 10);
            this.CharacterNameLabel.Name = "CharacterNameLabel";
            this.CharacterNameLabel.Size = new System.Drawing.Size(171, 25);
            this.CharacterNameLabel.TabIndex = 12;
            this.CharacterNameLabel.Text = "Character name:";
            this.CharacterNameLabel.Visible = false;
            // 
            // CharacterTypeLabel
            // 
            this.CharacterTypeLabel.AutoSize = true;
            this.CharacterTypeLabel.BackColor = System.Drawing.SystemColors.WindowText;
            this.CharacterTypeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.CharacterTypeLabel.ForeColor = System.Drawing.Color.Red;
            this.CharacterTypeLabel.Location = new System.Drawing.Point(24, 96);
            this.CharacterTypeLabel.MinimumSize = new System.Drawing.Size(10, 10);
            this.CharacterTypeLabel.Name = "CharacterTypeLabel";
            this.CharacterTypeLabel.Size = new System.Drawing.Size(159, 25);
            this.CharacterTypeLabel.TabIndex = 13;
            this.CharacterTypeLabel.Text = "Character type:";
            this.CharacterTypeLabel.Visible = false;
            // 
            // HpLabel
            // 
            this.HpLabel.AutoSize = true;
            this.HpLabel.BackColor = System.Drawing.SystemColors.WindowText;
            this.HpLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.HpLabel.ForeColor = System.Drawing.Color.Red;
            this.HpLabel.Location = new System.Drawing.Point(138, 139);
            this.HpLabel.MinimumSize = new System.Drawing.Size(10, 10);
            this.HpLabel.Name = "HpLabel";
            this.HpLabel.Size = new System.Drawing.Size(45, 25);
            this.HpLabel.TabIndex = 14;
            this.HpLabel.Text = "Hp:";
            this.HpLabel.Visible = false;
            // 
            // StrengtLabel
            // 
            this.StrengtLabel.AutoSize = true;
            this.StrengtLabel.BackColor = System.Drawing.SystemColors.WindowText;
            this.StrengtLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.StrengtLabel.ForeColor = System.Drawing.Color.Red;
            this.StrengtLabel.Location = new System.Drawing.Point(84, 176);
            this.StrengtLabel.MinimumSize = new System.Drawing.Size(10, 10);
            this.StrengtLabel.Name = "StrengtLabel";
            this.StrengtLabel.Size = new System.Drawing.Size(99, 25);
            this.StrengtLabel.TabIndex = 15;
            this.StrengtLabel.Text = "Strength:";
            this.StrengtLabel.Visible = false;
            // 
            // CharismaLabel
            // 
            this.CharismaLabel.AutoSize = true;
            this.CharismaLabel.BackColor = System.Drawing.SystemColors.WindowText;
            this.CharismaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.CharismaLabel.ForeColor = System.Drawing.Color.Red;
            this.CharismaLabel.Location = new System.Drawing.Point(74, 214);
            this.CharismaLabel.MinimumSize = new System.Drawing.Size(10, 10);
            this.CharismaLabel.Name = "CharismaLabel";
            this.CharismaLabel.Size = new System.Drawing.Size(109, 25);
            this.CharismaLabel.TabIndex = 16;
            this.CharismaLabel.Text = "Charisma:";
            this.CharismaLabel.Visible = false;
            // 
            // WisdomLabel
            // 
            this.WisdomLabel.AutoSize = true;
            this.WisdomLabel.BackColor = System.Drawing.SystemColors.WindowText;
            this.WisdomLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.WisdomLabel.ForeColor = System.Drawing.Color.Red;
            this.WisdomLabel.Location = new System.Drawing.Point(88, 251);
            this.WisdomLabel.MinimumSize = new System.Drawing.Size(10, 10);
            this.WisdomLabel.Name = "WisdomLabel";
            this.WisdomLabel.Size = new System.Drawing.Size(95, 25);
            this.WisdomLabel.TabIndex = 17;
            this.WisdomLabel.Text = "Wisdom:";
            this.WisdomLabel.Visible = false;
            // 
            // AgilityLabel
            // 
            this.AgilityLabel.AutoSize = true;
            this.AgilityLabel.BackColor = System.Drawing.SystemColors.WindowText;
            this.AgilityLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.AgilityLabel.ForeColor = System.Drawing.Color.Red;
            this.AgilityLabel.Location = new System.Drawing.Point(107, 285);
            this.AgilityLabel.MinimumSize = new System.Drawing.Size(10, 10);
            this.AgilityLabel.Name = "AgilityLabel";
            this.AgilityLabel.Size = new System.Drawing.Size(76, 25);
            this.AgilityLabel.TabIndex = 18;
            this.AgilityLabel.Text = "Agility:";
            this.AgilityLabel.Visible = false;
            // 
            // AgilityValueLabel
            // 
            this.AgilityValueLabel.AutoSize = true;
            this.AgilityValueLabel.BackColor = System.Drawing.SystemColors.WindowText;
            this.AgilityValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.AgilityValueLabel.ForeColor = System.Drawing.Color.Red;
            this.AgilityValueLabel.Location = new System.Drawing.Point(189, 285);
            this.AgilityValueLabel.MinimumSize = new System.Drawing.Size(10, 10);
            this.AgilityValueLabel.Name = "AgilityValueLabel";
            this.AgilityValueLabel.Size = new System.Drawing.Size(10, 25);
            this.AgilityValueLabel.TabIndex = 19;
            this.AgilityValueLabel.Visible = false;
            this.AgilityValueLabel.Click += new System.EventHandler(this.AgilityValueLabel_Click);
            // 
            // CharacterTypeComboBox
            // 
            this.CharacterTypeComboBox.BackColor = System.Drawing.SystemColors.WindowText;
            this.CharacterTypeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.CharacterTypeComboBox.ForeColor = System.Drawing.Color.Red;
            this.CharacterTypeComboBox.FormattingEnabled = true;
            this.CharacterTypeComboBox.Location = new System.Drawing.Point(43, 49);
            this.CharacterTypeComboBox.Name = "CharacterTypeComboBox";
            this.CharacterTypeComboBox.Size = new System.Drawing.Size(206, 33);
            this.CharacterTypeComboBox.TabIndex = 20;
            this.CharacterTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.CharacterTypeComboBox_SelectedIndexChanged);
            // 
            // CreateNewCharacterButton
            // 
            this.CreateNewCharacterButton.BackColor = System.Drawing.SystemColors.Desktop;
            this.CreateNewCharacterButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.CreateNewCharacterButton.ForeColor = System.Drawing.Color.Red;
            this.CreateNewCharacterButton.Location = new System.Drawing.Point(12, 7);
            this.CreateNewCharacterButton.Name = "CreateNewCharacterButton";
            this.CreateNewCharacterButton.Size = new System.Drawing.Size(173, 41);
            this.CreateNewCharacterButton.TabIndex = 21;
            this.CreateNewCharacterButton.Text = "New Character";
            this.CreateNewCharacterButton.UseVisualStyleBackColor = false;
            this.CreateNewCharacterButton.Visible = false;
            this.CreateNewCharacterButton.Click += new System.EventHandler(this.CreateNewCharacterButton_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.BackColor = System.Drawing.SystemColors.Desktop;
            this.SaveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.SaveButton.ForeColor = System.Drawing.Color.Red;
            this.SaveButton.Location = new System.Drawing.Point(293, 7);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(79, 41);
            this.SaveButton.TabIndex = 22;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = false;
            this.SaveButton.Visible = false;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // LoadButton
            // 
            this.LoadButton.BackColor = System.Drawing.SystemColors.Desktop;
            this.LoadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.LoadButton.ForeColor = System.Drawing.Color.Red;
            this.LoadButton.Location = new System.Drawing.Point(376, 7);
            this.LoadButton.Name = "LoadButton";
            this.LoadButton.Size = new System.Drawing.Size(79, 41);
            this.LoadButton.TabIndex = 23;
            this.LoadButton.Text = "Load";
            this.LoadButton.UseVisualStyleBackColor = false;
            this.LoadButton.Click += new System.EventHandler(this.LoadButton_Click);
            // 
            // DeleteButton
            // 
            this.DeleteButton.BackColor = System.Drawing.SystemColors.Desktop;
            this.DeleteButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.DeleteButton.ForeColor = System.Drawing.Color.Red;
            this.DeleteButton.Location = new System.Drawing.Point(12, 415);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(244, 32);
            this.DeleteButton.TabIndex = 24;
            this.DeleteButton.Text = "Delete this character";
            this.DeleteButton.UseVisualStyleBackColor = false;
            this.DeleteButton.Visible = false;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // UpdateNameButton
            // 
            this.UpdateNameButton.BackColor = System.Drawing.SystemColors.Desktop;
            this.UpdateNameButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.UpdateNameButton.ForeColor = System.Drawing.Color.Red;
            this.UpdateNameButton.Location = new System.Drawing.Point(12, 329);
            this.UpdateNameButton.Name = "UpdateNameButton";
            this.UpdateNameButton.Size = new System.Drawing.Size(244, 34);
            this.UpdateNameButton.TabIndex = 25;
            this.UpdateNameButton.Text = "Update name";
            this.UpdateNameButton.UseVisualStyleBackColor = false;
            this.UpdateNameButton.Visible = false;
            this.UpdateNameButton.Click += new System.EventHandler(this.UpdateNameButton_Click);
            // 
            // RenamingInputBox
            // 
            this.RenamingInputBox.BackColor = System.Drawing.SystemColors.WindowText;
            this.RenamingInputBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F);
            this.RenamingInputBox.ForeColor = System.Drawing.Color.Red;
            this.RenamingInputBox.Location = new System.Drawing.Point(12, 369);
            this.RenamingInputBox.Name = "RenamingInputBox";
            this.RenamingInputBox.Size = new System.Drawing.Size(244, 31);
            this.RenamingInputBox.TabIndex = 26;
            this.RenamingInputBox.Tag = "Character name...";
            this.RenamingInputBox.Visible = false;
            this.RenamingInputBox.TextChanged += new System.EventHandler(this.RenamingInputBox_TextChanged);
            // 
            // RPGCharacterCreation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(754, 459);
            this.Controls.Add(this.RenamingInputBox);
            this.Controls.Add(this.UpdateNameButton);
            this.Controls.Add(this.DeleteButton);
            this.Controls.Add(this.LoadButton);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.CreateNewCharacterButton);
            this.Controls.Add(this.CharacterTypeComboBox);
            this.Controls.Add(this.AgilityValueLabel);
            this.Controls.Add(this.AgilityLabel);
            this.Controls.Add(this.WisdomLabel);
            this.Controls.Add(this.CharismaLabel);
            this.Controls.Add(this.StrengtLabel);
            this.Controls.Add(this.HpLabel);
            this.Controls.Add(this.CharacterTypeLabel);
            this.Controls.Add(this.CharacterNameLabel);
            this.Controls.Add(this.WisdomValueLabel);
            this.Controls.Add(this.CharismaValueLabel);
            this.Controls.Add(this.StrenghtValueLabel);
            this.Controls.Add(this.HpValueLabel);
            this.Controls.Add(this.CharacterTypeValueLabel);
            this.Controls.Add(this.CharacterNameValueLabel);
            this.Controls.Add(this.CharacterCreateButton);
            this.Controls.Add(this.CharacterNameInput);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Name = "RPGCharacterCreation";
            this.Text = "RPG Character Creation";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox CharacterNameInput;
        private System.Windows.Forms.Button CharacterCreateButton;
        private System.Windows.Forms.Label CharacterNameValueLabel;
        private System.Windows.Forms.Label CharacterTypeValueLabel;
        private System.Windows.Forms.Label HpValueLabel;
        private System.Windows.Forms.Label StrenghtValueLabel;
        private System.Windows.Forms.Label CharismaValueLabel;
        private System.Windows.Forms.Label WisdomValueLabel;
        private System.Windows.Forms.Label CharacterNameLabel;
        private System.Windows.Forms.Label CharacterTypeLabel;
        private System.Windows.Forms.Label HpLabel;
        private System.Windows.Forms.Label StrengtLabel;
        private System.Windows.Forms.Label CharismaLabel;
        private System.Windows.Forms.Label WisdomLabel;
        private System.Windows.Forms.Label AgilityLabel;
        private System.Windows.Forms.Label AgilityValueLabel;
        private System.Windows.Forms.ComboBox CharacterTypeComboBox;
        private System.Windows.Forms.Button CreateNewCharacterButton;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Button LoadButton;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.Button UpdateNameButton;
        private System.Windows.Forms.TextBox RenamingInputBox;
    }
}

