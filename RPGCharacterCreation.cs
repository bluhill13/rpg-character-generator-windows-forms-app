﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace RPG_Character_Generator
{
    public partial class RPGCharacterCreation : Form
    {
        HelperClass helper = new HelperClass();
        private bool HaveCreatedCharacter { get; set; }
        public RPGCharacterCreation()
        {
            InitializeComponent();
            HaveCreatedCharacter = false;
        }

        /// <summary>
        /// Finds and updates path for where to stora and load json file
        /// and adding charactertype options to combobox and dictionary for later use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {

            string workingDirectory = Environment.CurrentDirectory;
            string projectDirectory = Directory.GetParent(workingDirectory).Parent.FullName;
            helper.JsonPath = projectDirectory + @"\Log\Character.json";
            foreach (var t in helper.characterTypes)
            {
                helper.CharacterTypeDict.Add(t.Name, t);
                CharacterTypeComboBox.Items.Add(t.Name);
            }
            //Setting a default in combobox
            CharacterTypeComboBox.SelectedIndex = CharacterTypeComboBox.FindString("W");
        }

        /// <summary>
        /// Creates a character, after finding its type from CharacterTypeDict
        /// </summary>
        private void CreateCharacter()
        {
            //Get type from dictionary with key from combobox
            helper.CharacterTypeDict.TryGetValue(CharacterTypeComboBox.SelectedItem.ToString(), out Type t);
            helper.Heroes.Add((Character)Activator.CreateInstance(t, CharacterNameInput.Text));
            //Change bool to later set the visibility
            HaveCreatedCharacter = true;
            //SetCharacterValues
            SetCharacterValues();
            //Change visibility
            ChangeVisibility();
        }
        /// <summary>
        /// Sets and updates Charactervalues to be shown in form
        /// </summary>
        private void SetCharacterValues()
        {
            CharacterNameValueLabel.Text = helper.Heroes.Last().Name;
            CharacterTypeValueLabel.Text = helper.Heroes.Last().Type;
            HpValueLabel.Text = helper.Heroes.Last().Hp.ToString();
            StrenghtValueLabel.Text = helper.Heroes.Last().Strength.ToString();
            CharismaValueLabel.Text = helper.Heroes.Last().Charsima.ToString();
            WisdomValueLabel.Text = helper.Heroes.Last().Wisdom.ToString();
            AgilityValueLabel.Text = helper.Heroes.Last().Agility.ToString();
        }
        /// <summary>
        /// Changes visibility to display mode or charactercreationmode
        /// depending on the value of HaveCreatedCharacterBool
        /// </summary>
        private void ChangeVisibility()
        {
            CharacterCreateButton.Visible = !HaveCreatedCharacter;
            CharacterNameInput.Visible = !HaveCreatedCharacter;
            CharacterTypeComboBox.Visible = !HaveCreatedCharacter;
            LoadButton.Visible = !HaveCreatedCharacter;
            RenamingInputBox.Visible = HaveCreatedCharacter;
            DeleteButton.Visible = HaveCreatedCharacter;
            UpdateNameButton.Visible = HaveCreatedCharacter;
            SaveButton.Visible = HaveCreatedCharacter;
            CreateNewCharacterButton.Visible = HaveCreatedCharacter;
            CharacterNameLabel.Visible = HaveCreatedCharacter;
            CharacterNameValueLabel.Visible = HaveCreatedCharacter;
            CharacterTypeLabel.Visible = HaveCreatedCharacter;
            CharacterTypeValueLabel.Visible = HaveCreatedCharacter;
            HpLabel.Visible = HaveCreatedCharacter;
            HpValueLabel.Visible = HaveCreatedCharacter;
            StrengtLabel.Visible = HaveCreatedCharacter;
            StrenghtValueLabel.Visible = HaveCreatedCharacter;
            CharismaLabel.Visible = HaveCreatedCharacter;
            CharismaValueLabel.Visible = HaveCreatedCharacter;
            WisdomLabel.Visible = HaveCreatedCharacter;
            WisdomValueLabel.Visible = HaveCreatedCharacter;
            AgilityLabel.Visible = HaveCreatedCharacter;
            AgilityValueLabel.Visible = HaveCreatedCharacter;
        }
        
        private void CharacterTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void CharacterNameInput_TextChanged(object sender, EventArgs e)
        {

        }
        private void RenamingInputBox_TextChanged(object sender, EventArgs e)
        {

        }

        #region Buttons
        private void Create_Click(object sender, EventArgs e)
        {
            if (!(CharacterNameInput.Text == ""))
            {
                CreateCharacter();
            }
            else
            {
                MessageBox.Show("Please write a name to create a character!");
            }

        }
        /// <summary>
        /// input for updating a character name
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateNewCharacterButton_Click(object sender, EventArgs e)
        {
            HaveCreatedCharacter = false;
            ChangeVisibility();
            CharacterNameInput.Text = "";
        }
        /// <summary>
        /// WHen load is clicked it will try to read and get the Json file in your 
        /// projectfolder/Log and create a new object in the correct type. THen update values
        /// ¨Since constructor only accepts a string for all objects.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadButton_Click(object sender, EventArgs e)
        {
            helper.GetCharacterFromDb(helper.GetLastIdData());
            HaveCreatedCharacter = true;
            //SetCharacterValues
            SetCharacterValues();
            //Change visibility
            ChangeVisibility();  
        }
        /// <summary>
        /// Will create and store the last element in the LIst containing characters
        /// and will only store that item in a json file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveButton_Click(object sender, EventArgs e)
        {
            helper.SetTableData();
            CharacterNameInput.Text = "";
            HaveCreatedCharacter = false;
            ChangeVisibility();
        }
        /// <summary>
        /// deletes a character from the database and updates
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteButton_Click(object sender, EventArgs e)
        {
            helper.DeleteCharacter(helper.GetLastIdData());
            HaveCreatedCharacter = false;
            ChangeVisibility();
        }
        /// <summary>
        /// Updates name and start database update aswell
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateNameButton_Click(object sender, EventArgs e)
        {
            if (!(RenamingInputBox.Text == "New name for character....") && !(RenamingInputBox.Text == ""))
            {
                helper.UpdateCharacter(helper.GetLastIdData(), RenamingInputBox.Text);
                HaveCreatedCharacter = true;
                SetCharacterValues();
                ChangeVisibility();
            }
            else
            {
                MessageBox.Show("Something went wrong! Try writing something in the messagebox below the button :)");
            }
        }
        #endregion

        #region LabelsFOrValues


        private void CharacterNameValueLabel_Click(object sender, EventArgs e)
        {

        }

        private void CharacterTypeValueLabel_Click(object sender, EventArgs e)
        {

        }

        private void HpValueLabel_Click(object sender, EventArgs e)
        {

        }

        private void StrenghtValueLabel_Click(object sender, EventArgs e)
        {

        }

        private void CharismaValueLabel_Click(object sender, EventArgs e)
        {

        }

        private void WisdomValueLabel_Click(object sender, EventArgs e)
        {

        }

        private void AgilityValueLabel_Click(object sender, EventArgs e)
        {

        }


        #endregion

        
    }
}
