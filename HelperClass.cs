﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Data.SqlClient;

namespace RPG_Character_Generator
{
    public class HelperClass
    {
        public  List<Character> Heroes = new List<Character>();

        public SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
        public string JsonPath { get; set; }
        
        public  List<Type> characterTypes = typeof(Character).Assembly.GetTypes()
                .Where(t => t.IsSubclassOf(typeof(Character)) && !t.IsAbstract).ToList();
        public  Dictionary<string, Type> CharacterTypeDict = new Dictionary<string, Type>();
        public HelperClass()
        {
            Heroes.Add(new Warrior("warrior-Test"));
            Heroes.Add(new Wizard("wizard-test"));
            //ConnectToDatabase();
            ConfigureCOnnectionString();
        }
        public void ConfigureCOnnectionString()
        {
            builder.DataSource = "PC7277\\SQLEXPRESS";
            builder.InitialCatalog = "RPG_Character_db";
            builder.IntegratedSecurity = true;
        }

        public void ConnectToDatabase()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    Console.WriteLine("Connected to database!");
                    //using (SqlDataReader reader = )
                    MessageBox.Show("Connected to database");

                }
            }catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        /// <summary>
        /// Opens a connection, and assigns and adds to 
        /// sql string to insert data into Character:table in database
        /// </summary>
        public void SetTableData()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    string sql = "INSERT INTO character_table VALUES(@Character_name, " +
                                                                        "@Hp," +
                                                                        "@Strenght," +
                                                                        "@Wisdom," +
                                                                        "@Charisma," +
                                                                        "@Agility," +
                                                                        "@ClassType," +
                                                                        "@Alive," +
                                                                        "@HumanShape)";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.Add("@Character_name", SqlDbType.NVarChar, 50).Value = Heroes.Last().Name;
                        command.Parameters.Add("@Hp", SqlDbType.Int).Value = Heroes.Last().Hp;
                        command.Parameters.Add("@Strenght", SqlDbType.Int).Value = Heroes.Last().Strength;
                        command.Parameters.Add("@Wisdom", SqlDbType.Int).Value = Heroes.Last().Wisdom;
                        command.Parameters.Add("@Charisma", SqlDbType.Int).Value = Heroes.Last().Charsima;
                        command.Parameters.Add("@Agility", SqlDbType.Int).Value = Heroes.Last().Agility;
                        command.Parameters.Add("@ClassType", SqlDbType.NVarChar, 50).Value = Heroes.Last().Type;
                        command.Parameters.Add("@Alive", SqlDbType.Bit).Value = Heroes.Last().Alive;
                        command.Parameters.Add("@HumanShape", SqlDbType.Bit).Value = Heroes.Last().HumanShape;
                        command.CommandType = CommandType.Text;
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Opens a connection, and assigns and adds to 
        /// sql string to insert data into Character_table in database
        /// </summary>
        public int GetLastIdData()
        {
            int Id = 1;
            try
            {
                
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    string sql = "SELECT TOP(1) ID FROM character_table ORDER BY 1 DESC";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader r = command.ExecuteReader())
                        {
                            r.Read();
                            Id = Convert.ToInt32(r["ID"]);
                            
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return Id;
        }
        /// <summary>
        /// Gets a character by id from database and updates List Heroes last item
        /// </summary>
        /// <param name="id"></param>
        public void GetCharacterFromDb(int id)
        {
            string typeString = "";
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    string sql = "SELECT* FROM character_table WHERE ID = @id";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.Add("@id", SqlDbType.Int).Value = id;

                        using (SqlDataReader r = command.ExecuteReader())
                        {
                            r.Read();
                            typeString = r["ClassType"].ToString();
                            typeString = typeString.Replace(" ", "");
                            CharacterTypeDict.TryGetValue(typeString.Split('(').First(), out Type t);
                            Heroes.Add((Character)Activator.CreateInstance(t, r["Character_name"].ToString()));
                            Heroes.Last().Hp = Convert.ToInt32(r["Hp"]);
                            Heroes.Last().Strength = Convert.ToInt32(r["Strenght"]);
                            Heroes.Last().Wisdom = Convert.ToInt32(r["Wisdom"]);
                            Heroes.Last().Charsima = Convert.ToInt32(r["Charisma"]);
                            Heroes.Last().Agility = Convert.ToInt32(r["Agility"]);
                            Heroes.Last().Alive = Convert.ToBoolean(r["Alive"]);
                            Heroes.Last().HumanShape = Convert.ToBoolean(r["HumanShape"]);
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        /// <summary>
        /// Deletes a character by id
        /// frontend uses getLastId function to always delete last item in database
        /// which is also displayed
        /// </summary>
        /// <param name="id"></param>
        public void DeleteCharacter(int id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    string sql = "DELETE FROM character_table WHERE ID = @id";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.Add("@id", SqlDbType.Int).Value = id;
                        command.CommandType = CommandType.Text;
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        /// <summary>
        /// Updates character name by id and 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="newName"></param>
        public void UpdateCharacter(int id, string newName)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    string sql = "UPDATE character_table SET Character_name = @newName WHERE ID = @id";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.Add("@id", SqlDbType.Int).Value = id;
                        command.Parameters.Add("newName", SqlDbType.NVarChar, 50).Value = newName;
                        command.CommandType = CommandType.Text;
                        command.ExecuteNonQuery();
                        //Update name value
                        Heroes.Last().Name = newName;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }


}

